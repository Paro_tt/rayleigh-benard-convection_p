module interpolation_pack

use global
use write_pack

contains

!::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
subroutine bilinear_interp(xc,yc,uyc,Tc)

real(dp),                  dimension(:),   intent(in) :: xc, yc
complex(C_DOUBLE_COMPLEX), dimension(:,:), intent(in) :: uyc, Tc
real(dp)                                              :: x1, x2, y1, y2
real(dp)                                              :: xi, eta
integer                                               :: ii, jj
integer                                               :: ic, jc

ic = 1 ! Counter
! Initialize x1 and x2
x1 = xc(ic)
x2 = xc(ic+1)
do ii = 1,Nx ! Loop over x
   if (ii == Nx) then
      ! Initialize x1 and x2
      x1 = xc(ic)
      x2 = abs(xc(1))
      xi = xp(ii) ! Locate x position on refined mesh
      jc = 1 ! Counter
      ! Initialize y1 and y2
      y1 = yc(jc)
      y2 = yc(jc+1)
      do jj = 1,Ny
         eta = yp(jj) ! Locate y position on refined mesh
         if (eta > y2) then! Check y interpolation points
            jc = jc + 1
            y1 = yc(jc)
            y2 = yc(jc+1)
         end if
         ! Set up coefficients for interpolation formula
         c0 = (x2 - x1)*(y2  - y1)
         c1 = (x2 - xi)*(y2  - eta)
         c2 = (x2 - xi)*(eta - y1)
         c3 = (xi - x1)*(y2  - eta)
         c4 = (xi - x1)*(eta - y1)
         ! Interpolate velocity
         uy(jj,ii) = (c1*uyc(jc,ic) + c2*uyc(jc+1,ic) + &
       &              c3*uyc(jc,1) + c4*uyc(jc+1,1)) / c0
         ! Interpolate temperature
         T(jj,ii) = (c1*Tc(jc,ic) + c2*Tc(jc+1,ic) + &
       &              c3*Tc(jc,1) + c4*Tc(jc+1,1)) / c0
      end do
   else
      xi = xp(ii) ! Locate x position on refined mesh
      if (xi > x2) then ! Check interpolation points
         ic = ic + 1
         x1 = xc(ic)
         x2 = xc(ic+1)
      end if
      jc = 1 ! Counter
      ! Initialize y1 and y2
      y1 = yc(jc)
      y2 = yc(jc+1)
      do jj = 1,Ny ! Loop over y
         eta = yp(jj) ! Locate y position on refined mesh
         if (eta > y2) then! Check y interpolation points
            jc = jc + 1
            y1 = yc(jc)
            y2 = yc(jc+1)
         end if
         ! Set up coefficients for interpolation formula
         c0 = (x2 - x1)*(y2  - y1)
         c1 = (x2 - xi)*(y2  - eta)
         c2 = (x2 - xi)*(eta - y1)
         c3 = (xi - x1)*(y2  - eta)
         c4 = (xi - x1)*(eta - y1)
         ! Interpolate velocity
         uy(jj,ii) = (c1*uyc(jc,ic) + c2*uyc(jc+1,ic) + &
       &              c3*uyc(jc,ic+1) + c4*uyc(jc+1,ic+1)) / c0
         ! Interpolate temperature
         T(jj,ii) = (c1*Tc(jc,ic) + c2*Tc(jc+1,ic) + &
       &              c3*Tc(jc,ic+1) + c4*Tc(jc+1,ic+1)) / c0
      end do
   end if 
end do

end subroutine bilinear_interp
!::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
subroutine linear_interp_x(xc,uyc,Tc)

real(dp),                  dimension(:),   intent(in) :: xc
complex(C_DOUBLE_COMPLEX), dimension(:,:), intent(in) :: uyc, Tc
real(dp)                                              :: x1, x2
real(dp)                                              :: xi
integer                                               :: ii
integer                                               :: ic

ic = 1 ! Counter
! Initialize x1 and x2
x1 = xc(ic)
x2 = xc(ic+1)
do ii = 1,Nx ! Loop over x
   xi = xp(ii) ! Locate x position on refined mesh
   if (ii == Nx) then
      ! Initialize x1 and x2
      x1 = xc(ic)
      x2 = abs(xc(1))
      ! Set up coefficients for interpolation formula
      c0 = (x2 - x1)
      c1 = (x2 - xi)
      c3 = (xi - x1)
      ! Interpolate velocity
      uy(:,ii) = (c1*uyc(:,ic) + c3*uyc(:,1)) / c0
      ! Interpolate temperature
      T(:,ii) = (c1*Tc(:,ic) + c3*Tc(:,1)) / c0
   else
      if (xi > x2) then ! Check interpolation points
         ic = ic + 1
         x1 = xc(ic)
         x2 = xc(ic+1)
      end if
         ! Set up coefficients for interpolation formula
         c0 = (x2 - x1)
         c1 = (x2 - xi)
         c3 = (xi - x1)
         ! Interpolate velocity
         uy(:,ii) = (c1*uyc(:,ic) + c3*uyc(:,ic+1)) / c0
         ! Interpolate temperature
         T(:,ii)  = (c1*Tc(:,ic)  + c3*Tc(:,ic+1))  / c0
   end if 
end do

end subroutine linear_interp_x
!::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
subroutine linear_interp_y(yc,uyc,Tc)

real(dp),                  dimension(:),   intent(in) :: yc
complex(C_DOUBLE_COMPLEX), dimension(:,:), intent(in) :: uyc, Tc
real(dp)                                              :: y1, y2
real(dp)                                              :: eta
integer                                               :: jj
integer                                               :: jc

jc = 1 ! Counter
! Initialize x1 and x2
y1 = yc(jc)
y2 = yc(jc+1)
do jj = 1,Ny ! Loop over x
   eta = yp(jj) ! Locate x position on refined mesh
   if (eta > y2) then ! Check interpolation points
      jc = jc + 1
      y1 = yc(jc)
      y2 = yc(jc+1)
   end if
   ! Set up coefficients for interpolation formula
   c0 = (y2 - y1)
   c1 = (y2 - eta)
   c2 = (eta - y1)
   ! Interpolate velocity
   uy(jj,:) = (c1*uyc(jc,:) + c2*uyc(jc+1,:)) / c0
   ! Interpolate temperature
   T(jj,:)  = (c1*Tc(jc,:)  + c2*Tc(jc+1,:))  / c0
end do

end subroutine linear_interp_y
!::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
subroutine quad_lagrange_interp(l1,l2,l3, x1,x2,x3,xi)

real(dp), intent(in)  :: xi, x1, x2, x3
real(dp), intent(out) :: l1, l2, l3

l1 = ((xi-x2) * (xi-x3)) / ((x1-x2) * (x1-x3))
l2 = ((xi-x1) * (xi-x3)) / ((x2-x1) * (x2-x3))
l3 = ((xi-x1) * (xi-x2)) / ((x3-x1) * (x3-x2))

end subroutine quad_lagrange_interp
!:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
subroutine expansion_operator

implicit none

integer               :: j
integer, dimension(3) :: sc
real(dp)              :: l1, l2, l3
real(dp)              :: x1, x2, x3, xi
real(dp)              :: t1, t2, t3

! Bring wall-normal fields to finer grid
sc = [1, 2, 3]
do j = 1,Nyf
   do ! Check stencil
      if (ypf(j) <= yp(sc(3))) then
         exit
      end if
      sc = sc + 1
   end do
   ! Interpolate between these three grid points
   x1 = yp(sc(1))
   x2 = yp(sc(2))
   x3 = yp(sc(3))

   ! Evaluate at this point
   xi = ypf(j)

   ! Quadratic lagrange interpolation
   call quad_lagrange_interp(l1,l2,l3, x1,x2,x3,xi)

   ! Interpolate function values
   t1 = tT_y(sc(1))
   t2 = tT_y(sc(2))
   t3 = tT_y(sc(3))
   tT_g2(j) = l1*t1 + l2*t2 + l3*t3

   t1 = tux_y(sc(1))
   t2 = tux_y(sc(2))
   t3 = tux_y(sc(3))
   tux_g2(j) = l1*t1 + l2*t2 + l3*t3

   t1 = tuy_y(sc(1))
   t2 = tuy_y(sc(2))
   t3 = tuy_y(sc(3))
   tuy_g2(j) = l1*t1 + l2*t2 + l3*t3

   t1 = nlT_y(sc(1))
   t2 = nlT_y(sc(2))
   t3 = nlT_y(sc(3))
   nlT_g2(j) = l1*t1 + l2*t2 + l3*t3

   t1 = tphi_y(sc(1))
   t2 = tphi_y(sc(2))
   t3 = tphi_y(sc(3))
   tphi_g2(j) = l1*t1 + l2*t2 + l3*t3

   t1 = nlphi_y(sc(1))
   t2 = nlphi_y(sc(2))
   t3 = nlphi_y(sc(3))
   nlphi_g2(j) = l1*t1 + l2*t2 + l3*t3
end do

end subroutine expansion_operator

!:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
subroutine restriction_operator(i)

implicit none

integer, intent(in)   :: i
integer               :: j
integer, dimension(3) :: sf
real(dp)              :: l1, l2, l3
real(dp)              :: x1, x2, x3, xi
real(dp)              :: t1, t2, t3

! Bring nonlinear fields to finer grid
sf = [1, 2, 3]
do j = 1,Ny
   do ! Check stencil
      if (yp(j) <= ypf(sf(3))) then
         exit
      end if
      sf = sf + 1
   end do
   ! Interpolate between these three grid points
   x1 = ypf(sf(1))
   x2 = ypf(sf(2))
   x3 = ypf(sf(3))

   ! Evaluate at this point
   xi = yp(j)

   ! Quadratic lagrange interpolation
   call quad_lagrange_interp(l1,l2,l3, x1,x2,x3,xi)

   ! Interpolate function values
   t1 = nlT_g2(sf(1))
   t2 = nlT_g2(sf(2))
   t3 = nlT_g2(sf(3))
   nlT(j,i) = l1*t1 + l2*t2 + l3*t3

   t1 = nlphi_g2(sf(1))
   t2 = nlphi_g2(sf(2))
   t3 = nlphi_g2(sf(3))
   nlphi(j,i) = l1*t1 + l2*t2 + l3*t3
end do

end subroutine restriction_operator

!:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
subroutine expansion_operator_ave

implicit none

integer               :: i, j
integer, dimension(3) :: sc
real(dp)              :: l1, l2, l3
real(dp)              :: x1, x2, x3, xi
real(dp)              :: t1, t2, t3

! Bring wall-normal fields to finer grid
do i = 1,Nx
   sc = [1, 2, 3]
   do j = 1,Nyf
      do ! Check stencil
         if (ypf(j) <= yp(sc(3))) then
            exit
         end if
         sc = sc + 1
      end do
      ! Interpolate between these three grid points
      x1 = yp(sc(1))
      x2 = yp(sc(2))
      x3 = yp(sc(3))

      ! Evaluate at this point
      xi = ypf(j)

      ! Quadratic lagrange interpolation
      call quad_lagrange_interp(l1,l2,l3, x1,x2,x3,xi)

      ! Interpolate function values
      t1 = uxi(sc(1), i)
      t2 = uxi(sc(2), i)
      t3 = uxi(sc(3), i)
      uxi_g2(j,i) = l1*t1 + l2*t2 + l3*t3

      t1 = uyi(sc(1), i)
      t2 = uyi(sc(2), i)
      t3 = uyi(sc(3), i)
      uyi_g2(j,i) = l1*t1 + l2*t2 + l3*t3
   end do
end do

end subroutine expansion_operator_ave

!:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
subroutine restriction_operator_ave

implicit none

integer               :: i, j
integer, dimension(3) :: sf
real(dp)              :: l1, l2, l3
real(dp)              :: x1, x2, x3, xi
real(dp)              :: t1, t2, t3

! Bring nonlinear fields to finer grid
do i = 1,Nx
   sf = [1, 2, 3]
   do j = 1,Ny
      do ! Check stencil
         if (yp(j) <= ypf(sf(3))) then
            exit
         end if
         sf = sf + 1
      end do
      ! Interpolate between these three grid points
      x1 = ypf(sf(1))
      x2 = ypf(sf(2))
      x3 = ypf(sf(3))

      ! Evaluate at this point
      xi = yp(j)

      ! Quadratic lagrange interpolation
      call quad_lagrange_interp(l1,l2,l3, x1,x2,x3,xi)

      ! Interpolate function values
      t1 = nlave_g2(sf(1), i)
      t2 = nlave_g2(sf(2), i)
      t3 = nlave_g2(sf(3), i)
      nlave(j,i) = l1*t1 + l2*t2 + l3*t3
   end do
end do

end subroutine restriction_operator_ave

!:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

end module interpolation_pack
